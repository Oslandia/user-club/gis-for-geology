### Description

*(Describe the proposed feature, which purpose does it have. You can eventually decline it into a list of identified actions.)*

### Benchmark

*(By the mean of one (or more) video(s) or picture(s), coming from either a research paper or an alternative tool, illustrate the targetted feature.)*

### Progress

*(If some work have already be done on the involved project, describe it there.)*

### Estimate budget

*(Select one of the following order of magnitude for estimated required budget.)*

- [ ] < 5k€
- [ ] >= 5k€, < 20k€
- [ ] >= 20k€

/label ~"Tool : Albion"
